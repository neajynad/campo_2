import { DiasProvider } from './../../providers/dias/dias';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  myDate:any;
  diaSeleccionadoBool = false;
  diasKeys:any = [];
  fechaDiaSeleccionado: any;
 
  constructor(public navCtrl: NavController, private _dias: DiasProvider, private toastCtrl: ToastController) {
    
  }

  mostrar(){
    this.diasKeys = []
    console.log(this.myDate);
    this._dias.getDia(this.myDate).then(()=>{
      if(Object.keys(this._dias.diaSeleccionado).length > 2){
        console.log(this._dias.diaSeleccionado)
        this.diaSeleccionadoBool = true;
        this.fechaDiaSeleccionado = this._dias.diaSeleccionado[0];
        for(let i = 0; i <  (Object.keys(this._dias.diaSeleccionado[2]).length); i++ ){
          if((this._dias.diaSeleccionado[2])[ (Object.keys(this._dias.diaSeleccionado[2]))[i]]){
            this.diasKeys.push(Object.keys(this._dias.diaSeleccionado[2])[i])
          }
        }
        console.log(this.diasKeys)
        //this.diasKeys = Object.keys(this._dias.diaSeleccionado[2]);
      }
      else if (Object.keys(this._dias.diaSeleccionado).length == 2){
        this.diaSeleccionadoBool = true;        
        this.fechaDiaSeleccionado = this._dias.diaSeleccionado[0];
      }
      else{
        let toast = this.toastCtrl.create({
          message: 'No tengo datos para ese día, prueba con otro correcto por favor.',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    })
  }

  prueba(){
    console.log("Entrando en prueba")
    this._dias.rellenarDiasTrabajador();
  }
}
