import { DiasProvider } from './../../providers/dias/dias';
import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, ModalController} from 'ionic-angular';
import { TrabajadoresProvider } from '../../providers/trabajadores/trabajadores';



@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  trabajadores:any = []
  constructor(public navCtrl: NavController, private _trabajadores: TrabajadoresProvider, private alertCtrl: AlertController, private toastCtrl: ToastController, private modalCtrl: ModalController, private _dias: DiasProvider) {
    
    this._trabajadores.getTrabajadores().then(()=>{
      console.log(this._trabajadores.trabajadores);
      this.trabajadores = this._trabajadores
    })
  }

  sumarDinero(trabajador:any){
    console.log("Prueba")
    let prompt = this.alertCtrl.create({
      title: 'Dinero a cuenta de ' + trabajador.nombre,
      inputs: [
        {
          name: 'dinero',
          placeholder: 'Introduce dinero'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Guardar',
          handler: data => {
            console.log(data);
            console.log(Number(data.dinero));
            if(!Number(data.dinero)){
              let toast = this.toastCtrl.create({
                message: 'La cantidad introducida no era correcta, prueba de nuevo por favor.',
                duration: 3000,
                position: 'top'
              });
              toast.present();
            }else{
              this._trabajadores.sumarDinero(trabajador.dinero + Number(data.dinero), trabajador.nombre)
            }
          }
        }
      ]
    });
    prompt.present();
  
  }

  addTrabajador(){
    let prompt = this.alertCtrl.create({
      title: 'Login',
      message: "Introduce un nombre y el dinero que ya lleva a cuenta",
      inputs: [
        {
          name: 'nombre',
          placeholder: 'Introduce un nombre'
        },
        {
          name: "dinero",
          placeholder: "Introduce dinero a cuenta"
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log(data);
            this._trabajadores.addTrabajador(Number(data.dinero), data.nombre)
          }
        }
      ]
    });
    prompt.present();
  }
  removeTrabajador(trabajador:any){
    let confirm = this.alertCtrl.create({
      title: 'Eliminar trabajador?',
      message: 'Estás seguro? Se perderán todos sus datos...',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this._trabajadores.deleteTrabajador(trabajador.nombre);
          }
        }
      ]
    });
    confirm.present();
  }

  editTrabajador(trabajador){
    let modal = this.modalCtrl.create("TrabajadorPage",{
      trabajador: trabajador
    });
    modal.present();
  }

  recalcular(){
    this._dias.rellenarDiasTrabajador();
  }
}
