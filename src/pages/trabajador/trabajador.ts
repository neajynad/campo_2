import { TrabajadoresProvider } from './../../providers/trabajadores/trabajadores';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

/**
 * Generated class for the TrabajadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:"TrabajadorPage"
})
@Component({
  selector: 'page-trabajador',
  templateUrl: 'trabajador.html',
})
export class TrabajadorPage {
  trabajador: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private _trabajadores: TrabajadoresProvider, private toastCtrl: ToastController, private alertCtrl:AlertController, private viewCtrl: ViewController) {
    this.trabajador = this.navParams.get("trabajador")
  }

  editDinero(trabajador){
    let prompt = this.alertCtrl.create({
      title: 'Dinero a cuenta de ' + trabajador.nombre,
      inputs: [
        {
          name: 'dinero',
          placeholder: 'Introduce dinero'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Guardar',
          handler: data => {
            console.log(data);
            console.log(Number(data.dinero));
            if(!Number(data.dinero)){
              let toast = this.toastCtrl.create({
                message: 'La cantidad introducida no era correcta, prueba de nuevo por favor.',
                duration: 3000,
                position: 'top'
              });
              toast.present();
            }else{
              this._trabajadores.sumarDinero( Number(data.dinero), trabajador.nombre);
              this.viewCtrl.dismiss();
            }
          }
        }
      ]
    });
    prompt.present();
  
  
  }

}
