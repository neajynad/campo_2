import { DiasProvider } from './../../providers/dias/dias';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { TrabajadoresProvider } from '../../providers/trabajadores/trabajadores';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myDate: any;
  trabajadores:any = [];
  dia:any = {};
  bool:boolean;
  nota:string ="";

  constructor(public navCtrl: NavController, private _trabajadores: TrabajadoresProvider, private _dias:DiasProvider, private alertCtrl:AlertController) {
    let timeNow = new Date().getDate();
    console.log(timeNow);
    this._trabajadores.getTrabajadores().then(()=>{
      console.log(this._trabajadores.trabajadores);
      this.trabajadores = this._trabajadores;
    })
  }

  mostrar(){
    console.log(this.dia)
  }

  addDia(date:any){
    console.log(this.nota)
    let confirm = this.alertCtrl.create({
      title: 'Añadir los trabajadores a la fecha ' + this.myDate + '?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this._dias.addDia(date,this.dia, this.nota);
          }
        }
      ]
    });
    confirm.present();
  }

  
}
