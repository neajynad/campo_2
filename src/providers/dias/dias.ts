import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { TrabajadoresProvider } from '../trabajadores/trabajadores';

/*
  Generated class for the DiasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DiasProvider {
  dias:any;
  diaSeleccionado:any;

  constructor(private afDB: AngularFireDatabase, private _trabajadores: TrabajadoresProvider) {
    this.getDias();
  }

  addDia(date:any, diaTrabajadores:any, nota:string){
    console.log(diaTrabajadores)
    this.afDB.list("BaseDias").set(date,{
      trabajadores:diaTrabajadores,
      fecha: date,
      nota: nota
    });

    for(let i = 0; i < Object.keys(diaTrabajadores).length; i++){
      if(diaTrabajadores[Object.keys(diaTrabajadores)[i]]){
        console.log(Object.keys(diaTrabajadores)[i]);
        this._trabajadores.addDayTrabajador(Object.keys(diaTrabajadores)[i]);
      }
    }
  }

  getDias(){
    let promesa = new Promise((resolve,reject)=>{
    this.afDB.list('BaseDias').valueChanges().subscribe((data)=>{
        this.dias = data
        resolve();
      });
    });
    return promesa;
    
  }

  getDia(date){
    console.log("Entrando en getDia...")
    let promesa = new Promise((resolve,reject)=>{
      this.afDB.list('BaseDias/' + date).valueChanges().subscribe((data)=>{
          console.log(data);
          this.diaSeleccionado = data
          resolve();
        });
      });
      return promesa;

  }

  rellenarDiasTrabajador(){
    let objeto = new Object();
    for(let dia of this.dias){
      console.log(dia.trabajadores)
      if( (dia.trabajadores) ){
        for(let i=0; i<Object.keys(dia.trabajadores).length; i++){
          console.log(Object.keys(dia.trabajadores)[i])
          //this._trabajadores.addDays(Object.keys(dia.trabajadores)[i]);
          if(!objeto[(Object.keys(dia.trabajadores)[i])]){
            objeto[(Object.keys(dia.trabajadores)[i])] =1           
          }
          else if(dia.trabajadores[Object.keys(dia.trabajadores)[i]]){
            console.log(dia.trabajadores[Object.keys(dia.trabajadores)[i]])
            objeto[(Object.keys(dia.trabajadores)[i])] +=1 
          }
        }
      }
      console.log(objeto)
      for(let j=0; j < Object.keys(objeto).length; j++){
        console.log(Object.keys(objeto)[j]);
        console.log(objeto[Object.keys(objeto)[j]])
        this._trabajadores.addDays(Object.keys(objeto)[j], objeto[Object.keys(objeto)[j]] );      
      }
      }
  }
}
