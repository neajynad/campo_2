import { DiasProvider } from './../dias/dias';
import { Injectable } from '@angular/core';


import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


/*
  Generated class for the TrabajadoresProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrabajadoresProvider {
  items: Observable<any[]>;
  trabajadores: any = [];

  constructor( private afDB: AngularFireDatabase) {

  }

  getTrabajadores(){
    let promesa = new Promise((resolve,reject)=>{
      this.afDB.list('BaseTrabajadores').valueChanges().subscribe((data)=>{
        this.trabajadores = data
        resolve();
      });
    });
    return promesa;
    
  }

  getDias(nombreTrabajador){
    let number;
    let promesa = new Promise((resolve,reject)=>{
      this.afDB.list('BaseTrabajadores/'+ nombreTrabajador).valueChanges().subscribe((data:any)=>{
        number = data[0];
        resolve(number);
      });

    })
    return promesa
  }

  sumarDinero(dinero:number, nombreTrabajador:string){
    this.afDB.list('BaseTrabajadores').update(nombreTrabajador,{
      dinero: dinero
    })
  }

  addTrabajador( dinero:number, nombreTrabajador:string){
    this.afDB.list("BaseTrabajadores").set(nombreTrabajador,{
      dias : 0,
      dinero: dinero,
      nombre: nombreTrabajador
    })
  }

  deleteTrabajador(nombreTrabajador:string){
    this.afDB.list("BaseTrabajadores").remove(nombreTrabajador);
  }

  addDayTrabajador(nombreTrabajador:string){
    let dias
    this.getDias(nombreTrabajador).then((data)=>{
      dias = data;
        console.log(nombreTrabajador + " "+ dias)
          this.afDB.list("BaseTrabajadores").update(nombreTrabajador,{
            dias: dias + 1
            });
      });
    
  }
  addDays(nombreTrabajador:string, dias){
        console.log(nombreTrabajador + " "+ dias)
          this.afDB.list("BaseTrabajadores").update(nombreTrabajador,{
            dias: dias 
            });
  }
  
}
